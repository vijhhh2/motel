import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loadedFeature = 'recipe';

  ngOnInit() {
  firebase.initializeApp({
    apiKey: 'AIzaSyDcVRBnM-vozQwso50o-108Pd_wUhp47r4',
    authDomain: 'recipie-book.firebaseapp.com'
  });
  }

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }
}
