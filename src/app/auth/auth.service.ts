import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  token: string;

  constructor(private router: Router) { }
  signUpusers(email: string, password: string) {
  firebase.auth().createUserWithEmailAndPassword(email, password)
  .catch(
    error => console.log(error)
  );
  }
  signInUsers(email: string, password: string) {
    firebase.auth().signInWithEmailAndPassword(email, password)
    .then(
      (response) => {
        this.router.navigate(['/']);
        firebase.auth().currentUser.getIdToken()
        .then(
          (Token: string) => this.token = Token
        );
      }
    )
    .catch(
      (error) => console.log(error)
    );
  }
  getToken() {
    firebase.auth().currentUser.getIdToken()
    .then(
      (Token: string) => this.token = Token
    );
    return this.token;
  }
  isAuthenticated() {
    return this.token != null;
  }
  logOut() {
    firebase.auth().signOut();
    this.token = null;
  }
}
