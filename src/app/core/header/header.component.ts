import { Component } from '@angular/core';
import { Response } from '@angular/http';
// import { HttpEvent, HttpEventType } from '@angular/common/http';


import { DataStorageService } from '../../data-storage.service';
import { RecipeService } from '../../recipes/recipe.service';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  constructor(private dsService: DataStorageService, private recipeService: RecipeService,
  private authService: AuthService, private router: Router) {}
  onSave() {
    this.dsService.saveRecipies()
    .subscribe(
      (response) => {
        console.log(response);
      }
    );
  }
  onFetch() {
    this.dsService.fetchRecipes();
  }
  onLogout() {
    this.authService.logOut();
    this.router.navigate(['/']);
  }
  isAuthenticated() {
    return this.authService.isAuthenticated();
  }
}

