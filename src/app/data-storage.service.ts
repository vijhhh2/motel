import { Recipe } from './recipes/recipe.model';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';


import { RecipeService } from './recipes/recipe.service';
import { AuthService } from './auth/auth.service';


@Injectable()
export class DataStorageService {

  constructor(private httpClient: HttpClient, private recipieService: RecipeService,
  private authService: AuthService) { }
  saveRecipies() {

    // const headers = new HttpHeaders().set('Autharization', 'barere token');
  //   return this.httpClient.put('https://recipie-book.firebaseio.com/recipes.json', this.recipieService.getRecipes(),
  // {observe: 'body',
  // params: new HttpParams().set('auth', token)
  // headers: headers
  const req = new HttpRequest('PUT', 'https://recipie-book.firebaseio.com/recipes.json', this.recipieService.getRecipes(),
);
return this.httpClient.request(req);
  }
  fetchRecipes() {
    return this.httpClient.get<Recipe[]>('https://recipie-book.firebaseio.com/recipes.json', {
      observe: 'body',
      responseType: 'json',
    })
    .map(
      (recipes) => {
        // console.log(recipes);
        for (const recipie of recipes) {
          if (!recipie['ingredients']) {
            recipie['ingredients'] = [];
          }
        }
        return recipes;
        // return  [] ;
      }
    )
  .subscribe(
    (recipes: Recipe[]) => {
      console.log(recipes);
      this.recipieService.setRecipies(recipes);
    }
  );
  }
}
